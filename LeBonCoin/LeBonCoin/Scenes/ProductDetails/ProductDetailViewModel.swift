//
//  ProductDetailViewModel.swift
//  LeBonCoin
//
//  Created by Malek Mansour on 09/02/2021.
//

import Foundation
import UIKit

protocol DetailViewModelProtocol {
    var product: ProductProtocol? { get set }
}
class DetailViewModel: DetailViewModelProtocol {

    var product: ProductProtocol?

    init(item: ProductProtocol) {
        self.product = item
    }
}
