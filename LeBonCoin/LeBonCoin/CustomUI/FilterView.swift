//
//  DropMenuButton.swift
//  Aircall
//
//  Created by Malek Mansour on 09/02/2021.
//

import UIKit

class FilterView: UIView {

    weak var delegate: SelectFilterDelegate?
    var items = ["Toute les annonces"]
    var table = UITableView()

    func appendTableViewList(_ items: [String]) {
        self.items.append(contentsOf: items)
        self.addSubview(table)
        table.delegate = self
        table.dataSource = self
        table.translatesAutoresizingMaskIntoConstraints = false
        table.topAnchor.constraint(equalTo: topAnchor).isActive = true
        table.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        table.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        table.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        table.reloadData()
    }
}

extension FilterView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.delegate?.didSelectNewCategory(category: items[indexPath.row])
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
        cell.textLabel?.text = items[(indexPath as NSIndexPath).row]
        return cell
    }
}
