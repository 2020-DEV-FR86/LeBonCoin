//
//  MasterViewModel.swift
//  LeBonCoin
//
//  Created by Malek Mansour on 09/02/2021.
//

import Foundation
import UIKit

protocol MainViewModelLayoutLogic {
    func layoutProductCell(on cell: UITableViewCell, with product: ProductProtocol)
}
protocol MainViewModelBusinessLogic: class {
    var allProductsList: [ProductProtocol]? { get set }
    var displayableProductsList: [ProductProtocol]? { get set }
    var categoryList: [CategoryProtocol]? { get set }
    func loadData(completionHandler: (() -> Void)?)
    func filter(with categoryTitle: String, completionHandler: (() -> Void)?)
}
class MainViewModel: MainViewModelBusinessLogic, MainViewModelLayoutLogic {

    var allProductsList: [ProductProtocol]?
    var displayableProductsList: [ProductProtocol]?
    var categoryList: [CategoryProtocol]?

    func loadData(completionHandler: (() -> Void)? = nil) {
        let dispatch = DispatchGroup()
        dispatch.enter()
        APIWrapper.downloadCategories { (categories) in
            self.categoryList = categories
            dispatch.leave()
        }
        dispatch.notify(queue: .global()) {
            APIWrapper.getProductsList { (list) in
                self.displayableProductsList = list.map { (item) -> ProductProtocol in
                    var product = item
                    product.categoryTitle = self.categoryList?.filter({ $0.identifier == item.categoryId }).first?.name
                    return product
                }.sorted { (pOne, pTwo) -> Bool in
                    guard let firstDate = pOne.date, let secondDate = pTwo.date else { return true}
                    return secondDate >= firstDate
                }
                self.allProductsList = self.displayableProductsList
                guard completionHandler != nil else { return}
                completionHandler!()
            }
        }
    }

    func filter(with categoryTitle: String, completionHandler: (() -> Void)? = nil) {
        if categoryTitle == "Toute les annonces" {
            displayableProductsList = allProductsList
        } else {
            displayableProductsList = allProductsList?.filter({ (item) -> Bool in
                         return item.categoryTitle == categoryTitle
                     })
        }
        guard completionHandler != nil else { return}
        completionHandler!()
    }

    func layoutProductCell(on cell: UITableViewCell, with product: ProductProtocol) {
        cell.imageView?.image = product.thumbImage
        cell.imageView?.contentMode = .scaleAspectFit
        cell.textLabel?.text = product.title
        cell.textLabel?.numberOfLines = 2
        cell.selectionStyle = .none
        let catString = "Catégorie: \(product.categoryTitle.orValue("")) \n"
        cell.detailTextLabel?.text = catString+"Prix: \(product.price.orValue(0))€"
        cell.detailTextLabel?.numberOfLines = 2
        _ = product.isUrgent == true ? cell.contentView.addUrgentBadge() : ()
    }
}
