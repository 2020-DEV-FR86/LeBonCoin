//
//  JSONCodingMapperTests.swift
//  LeBonCoinTests
//
//  Created by Malek Mansour on 17/02/2021.
//

import XCTest

@testable import LeBonCoin
// swiftlint:disable line_length
class JSONCodingMapperTests: XCTestCase {

    static let jsonString = "{\"id\": 1,\"category_id\": 1,\"title\": \"title\", \"description\": \"desc\",\"price\": 140,\"images_url\": {\"small\": \"small.jpg\",\"thumb\": \"thumb.jpg\"},\"creation_date\": \"now\",\"is_urgent\": false}"

    private let fixture = Data(jsonString.utf8)
    private let fixtureMissingAttributes = Data("{\"title\": \"square\"}".utf8)

    func testDecoding_whenSquare_returnsAProduct() throws {
        XCTAssertNotNil(try JSONDecoder().decode(Product.self, from: fixture))
    }

    func testDecoding_whenMissingAttributes_itThrows() {
        assertThrowsKeyNotFound("id", decoding: Product.self, from: fixtureMissingAttributes)
    }

    private func assertThrowsKeyNotFound<T: Decodable>(_ expectedKey: String,
                                                       decoding: T.Type,
                                                       from data: Data,
                                                       file: StaticString = #file,
                                                       line: UInt = #line) {
        XCTAssertThrowsError(try JSONDecoder().decode(decoding, from: data), file: file, line: line) { error in
            if case .keyNotFound(let key, _)? = error as? DecodingError {
                XCTAssertEqual(expectedKey, key.stringValue, "Expected missing key '\(key.stringValue)' to equal '\(expectedKey)'.",
                               file: file, line: line)
            } else {
                XCTFail("Expected '.keyNotFound(\(expectedKey))' but got \(error)", file: file, line: line)
            }
        }
    }
}
