//
//  JSONCodable.swift
//  LeBonCoin
//
//  Created by Malek Mansour on 17/02/2021.
//

import Foundation

protocol JSONCodable {}

extension JSONCodable where Self: Encodable {
    var jsonDictionary: [String: Any]? {
        guard let data = jsonData,
            let dictionary = try? JSONSerialization.jsonObject(
                with: data, options: .allowFragments
            ) as? [String: Any] else { return nil }
        return dictionary
    }

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }
}

extension JSONCodable where Self: Decodable {
    init(from data: Data) throws {
        self = try JSONDecoder().decode(Self.self, from: data)
    }

    init(from dictionary: [String: Any]) throws {
        let data = try JSONSerialization.data(withJSONObject: dictionary, options: [])
        try self.init(from: data)
    }
}
