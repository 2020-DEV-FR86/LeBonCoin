//
//  FontStyle.swift
//  LeBonCoin
//
//  Created by Malek Mansour on 09/02/2021.
//

import Foundation
import UIKit

enum FontStyle {

    case regular
    case title
    case detail

    var font: UIFont {
        switch self {
        case .regular:
            return UIFont.systemFont(ofSize: 14)
        case .title:
            return UIFont.boldSystemFont(ofSize: 20)
        case .detail:
            return UIFont.systemFont(ofSize: 12)
        }
    }
}
