//
//  Products.swift
//  LeBonCoin
//
//  Created by Malek Mansour on 09/02/2021.
//

import Foundation
import UIKit

protocol ProductProtocol: Decodable {
    var identifier: Int { get }
    var categoryId: Int { get set }
    var categoryTitle: String? { get set }
    var title: String? { get set }
    var description: String? { get set }
    var price: Int? { get set }
    var imageUrl: Images? { get set }
    var creationDate: String? { get set }
    var isUrgent: Bool? { get set }
    var date: Date? { get}
    var smallImage: UIImage? { get}
    var thumbImage: UIImage? { get}
}
struct Product: ProductProtocol, JSONCodable, Equatable {

    var identifier: Int
    var categoryId: Int
    var categoryTitle: String?
    var title: String?
    var description: String?
    var price: Int?
    var imageUrl: Images?
    var creationDate: String?
    var isUrgent: Bool?
    var date: Date? {
        let formater = DateFormatter()
        formater.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        guard let dateString = creationDate else { return Date() }
        return formater.date(from: dateString)
    }
    var smallImage: UIImage?
    var thumbImage: UIImage?

    enum CodingKeys: String, CodingKey {
        case identifier = "id"
        case categoryId = "category_id"
        case title
        case description
        case price
        case imageUrl = "images_url"
        case creationDate = "creation_date"
        case isUrgent = "is_urgent"
    }

    public init(from decoder: Decoder) throws {
        let value = try decoder.container(keyedBy: CodingKeys.self)
        identifier = try value.decode(Int.self, forKey: .identifier)
        categoryId = try value.decode(Int.self, forKey: .categoryId)
        title = try? value.decode(String.self, forKey: .title)
        description = try? value.decode(String.self, forKey: .description)
        price = try? value.decode(Int.self, forKey: .price)
        imageUrl = try? value.decode(Images.self, forKey: .imageUrl)
        creationDate = try? value.decode(String.self, forKey: .creationDate)
        isUrgent = try? value.decode(Bool.self, forKey: .isUrgent)
        if let thumbString = imageUrl?.thumb {
            thumbImage = UIImageView.load(from: thumbString)
        }
    }
    static func == (lhs: Product, rhs: Product) -> Bool {
        return lhs.identifier == rhs.identifier
    }
}
struct Images: Decodable {
    var small: String
    var thumb: String
}
