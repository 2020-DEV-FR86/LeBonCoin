//
//  ProductDetailsViewController.swift
//  LeBonCoin
//
//  Created by Malek Mansour on 09/02/2021.
//

import UIKit

class ProductDetailsViewController: UIViewController {

    var viewModel: DetailViewModelProtocol?

    lazy var mainStack = setStackViewContainer()
    lazy var smallImg = setSmallImage()
    lazy var titleLbel = setLabel(with: 2, style: .title)
    lazy var categoryLbel = setLabel(with: 1, style: .detail)
    lazy var dateLabel = setLabel(with: 1, style: .detail)
    lazy var priceLbel = setLabel(with: 1, style: .regular)
    lazy var descTextView = setTextView(with: .regular)

    internal static func instantiate(with viewModel: DetailViewModelProtocol) -> ProductDetailsViewController {
        let viewController = ProductDetailsViewController()
        viewController.viewModel = viewModel
        return viewController
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        fillAndLayoutView()
    }

    private func fillAndLayoutView() {
        setViewHierarchy()
        setConstraints()
        if let smallString = viewModel?.product?.imageUrl?.small {
            smallImg.image = UIImageView.load(from: smallString)
        }
        titleLbel.text = viewModel?.product?.title
        descTextView.text = viewModel?.product?.description
        guard let category = viewModel?.product?.categoryTitle.orValue(""),
            let price = viewModel?.product?.price.orValue(0),
            let date = viewModel?.product?.date else { return }
        categoryLbel.text = "Catégorie: \(String(describing: category))"
        priceLbel.text = "Prix: \(String(describing: price)) €"
        _ = viewModel?.product?.isUrgent == true ? smallImg.addUrgentBadge() : ()
        dateLabel.text = "Publiée: le \(date.dayAsString) \(date.day) \(date.monthAsString) \(date.year)"
        self.view.backgroundColor = .white
    }
}
// MARK: Private fcts
private extension ProductDetailsViewController {

    func setSmallImage() -> UIImageView {
        let imgView = UIImageView(frame: .zero)
        imgView.contentMode = .scaleAspectFit
        return imgView
    }
    func setTextView(with style: FontStyle) -> UITextView {
        let textView = UITextView(frame: .zero)
        textView.font = style.font
        return textView
    }
    func setLabel(with numberOfLines: Int, style: FontStyle) -> UILabel {
        let label = UILabel(frame: .zero)
        label.backgroundColor = .white
        label.textColor = .black
        label.numberOfLines = numberOfLines
        label.textAlignment = .left
        label.font = style.font
        return label
    }
    func setStackViewContainer() -> UIStackView {
        let stack = UIStackView(frame: .zero)
        stack.distribution = .fillProportionally
        stack.axis = .vertical
        stack.backgroundColor = .white
        return stack
    }

    private func setConstraints() {
        mainStack.translatesAutoresizingMaskIntoConstraints = false
        mainStack.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 0).isActive = true
        mainStack.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 8).isActive = true
        mainStack.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -8).isActive = true

        smallImg.translatesAutoresizingMaskIntoConstraints = false
        smallImg.heightAnchor.constraint(equalToConstant: 180).isActive = true

        titleLbel.translatesAutoresizingMaskIntoConstraints = false
        titleLbel.heightAnchor.constraint(greaterThanOrEqualToConstant: 40).isActive = true

        dateLabel.translatesAutoresizingMaskIntoConstraints = false
        dateLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true

        categoryLbel.translatesAutoresizingMaskIntoConstraints = false
        categoryLbel.heightAnchor.constraint(greaterThanOrEqualToConstant: 20).isActive = true

        priceLbel.translatesAutoresizingMaskIntoConstraints = false
        priceLbel.heightAnchor.constraint(greaterThanOrEqualToConstant: 20).isActive = true

        descTextView.translatesAutoresizingMaskIntoConstraints = false
        descTextView.heightAnchor.constraint(greaterThanOrEqualToConstant: 80).isActive = true
    }
    private func setViewHierarchy() {
        self.title = viewModel?.product?.title
        self.view.addSubview(mainStack)
        mainStack.addArrangedSubview(smallImg)
        mainStack.addArrangedSubview(titleLbel)
        mainStack.addArrangedSubview(dateLabel)
        mainStack.addArrangedSubview(categoryLbel)
        mainStack.addArrangedSubview(descTextView)
        mainStack.addArrangedSubview(priceLbel)
    }
}
