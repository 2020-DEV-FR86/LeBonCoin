//
//  LeBonCoinTests.swift
//  LeBonCoinTests
//
//  Created by Malek Mansour on 08/02/2021.
//

import XCTest
@testable import LeBonCoin

class LeBonCoinTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testRemoteData() {
        XCTAssertEqual(APIWrapper.listProductUrl,
                       "https://raw.githubusercontent.com/leboncoin/paperclip/master/listing.json")
        XCTAssertEqual(APIWrapper.categoriesListUrl,
                       "https://raw.githubusercontent.com/leboncoin/paperclip/master/categories.json")
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
}
