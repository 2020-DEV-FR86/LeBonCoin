//
//  Extension.swift
//  LeBonCoin
//
//  Created by Malek Mansour on 09/02/2021.
//

import Foundation
import UIKit

// MARK: UIImzge
extension UIImageView {
    static func load(from urlString: String) -> UIImage? {
        guard let url = URL(string: urlString),
            let imageData = try? Data(contentsOf: url, options: .dataReadingMapped),
            let bgImage = UIImage(data: imageData) else { return nil}
        return bgImage
    }
}
// MARK: Optional
extension Optional {
    func orValue(_ value: Wrapped) -> Wrapped {
        if let data = self {
            return data
        } else {
            return value
        }
    }
}
// MARK: Date
extension Date {
    var day: Int {
        let calendar = Calendar.current
        return calendar.component(.day, from: self)
    }
    var year: Int {
        let calendar = Calendar.current
        return calendar.component(.year, from: self)
    }

    var dayAsString: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        return dateFormatter.string(from: self)
    }

    var monthAsString: String {
        let dateformat = DateFormatter()
        dateformat.setLocalizedDateFormatFromTemplate("MMM")
        return dateformat.string(from: self)
    }

}
// MARK: UITView
extension UIView {
    func addUrgentBadge() {
        let label = UILabel(frame: CGRect(origin: CGPoint(x: UIScreen.main.bounds.width-50, y: 20),
                                         size: CGSize(width: 30, height: 30)))
        label.text = "!"
        label.textAlignment = .center
        label.backgroundColor = .orange
        label.layer.cornerRadius = 10
        label.layer.masksToBounds = true
        self.addSubview(label)
    }
}
// MARK: UILabel
extension UILabel {

    func setLabelStyle(with numberOfLines: Int, style: FontStyle) {
        self.backgroundColor = .white
        self.textColor = .black
        self.numberOfLines = numberOfLines
        self.textAlignment = .left
        self.font = style.font
    }

}
