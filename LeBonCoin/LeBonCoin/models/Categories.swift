//
//  Categories.swift
//  LeBonCoin
//
//  Created by Malek Mansour on 09/02/2021.
//

import Foundation

protocol CategoryProtocol: Decodable {
    var identifier: Int { get }
    var name: String? { get set}
}
public struct Category: CategoryProtocol {
    var identifier: Int
    var name: String?

    enum CodingKeys: String, CodingKey {
        case identifier = "id"
        case name
    }
}
