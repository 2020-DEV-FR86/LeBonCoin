//
//  MainViewController.swift
//  LeBonCoin
//
//  Created by Malek Mansour on 09/02/2021.
//

import UIKit

protocol SelectFilterDelegate: class {
    func didSelectNewCategory(category: String)
}
class MainViewController: UIViewController {

    lazy var blurEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
    lazy var tableView = setTableView()
    lazy var loaderIndicator = setIndicatorView()
    lazy var filterView = setFilterView()

    var viewModel: (MainViewModelBusinessLogic & MainViewModelLayoutLogic)?

    override func viewDidLoad() {
        super.viewDidLoad()
        setViewHierarchy()
        setConstraints()
        viewModel?.loadData { [weak self] in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
                self?.loaderIndicator.stopAnimating()
                self?.configNavigationBar()
            }
        }
    }
}
private extension MainViewController {
    func configNavigationBar() {
        self.title = "Liste d'annonces"
        let button = UIButton(type: .custom)
        button.addTarget(self, action: #selector(openFilterView), for: .touchUpInside)
        button.setTitle("Filtrer", for: .normal)
        button.setTitleColor(.black, for: .normal)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: button)
    }
    func setConstraints() {
        tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        tableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        tableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true

        loaderIndicator.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        loaderIndicator.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        loaderIndicator.widthAnchor.constraint(equalToConstant: 60).isActive = true
        loaderIndicator.heightAnchor.constraint(equalToConstant: 60).isActive = true

        filterView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        filterView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        filterView.widthAnchor.constraint(equalToConstant: 220).isActive = true
        filterView.heightAnchor.constraint(equalToConstant: 300).isActive = true
    }
    func setViewHierarchy() {
        self.title = "Chargement…"
        self.view.addSubview(tableView)
        self.view.addSubview(loaderIndicator)
        loaderIndicator.startAnimating()
        self.view.addSubview(filterView)
        self.view.sendSubviewToBack(filterView)
    }
    @objc
    func openFilterView() {
        guard let list = viewModel?.categoryList else { return }
        filterView.appendTableViewList(list.compactMap { $0.name })
        let tap = UITapGestureRecognizer(target: self, action: #selector(removefilterView))
        if !UIAccessibility.isReduceTransparencyEnabled {
            blurEffectView.frame = self.view.bounds
            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            blurEffectView.addGestureRecognizer(tap)
            view.addSubview(blurEffectView)
        } else {
            view.backgroundColor = .black
        }
        self.view.bringSubviewToFront(filterView)
    }
    @objc
    func removefilterView(_ sender: UITapGestureRecognizer?) {
        self.view.sendSubviewToBack(filterView)
        blurEffectView.removeFromSuperview()
    }
}
// MARK: SelectFilterDelegate
extension MainViewController: SelectFilterDelegate {
    func didSelectNewCategory(category: String) {
        removefilterView(nil)
        viewModel?.filter(with: category) {
            self.tableView.reloadData()
        }
    }
}
// MARK: UITableViewDelegate, UITableViewDataSource
extension MainViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel?.displayableProductsList?.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: "cell")
        guard let product = self.viewModel?.displayableProductsList?[indexPath.row] else { return cell}
        viewModel?.layoutProductCell(on: cell, with: product)
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let product = self.viewModel?.displayableProductsList?[indexPath.row] else { return}
        let detailViewModel = DetailViewModel(item: product)
        let detailViewController = ProductDetailsViewController.instantiate(with: detailViewModel)
        show(detailViewController, sender: nil)
    }
}
// MARK: lazy outLet
private extension MainViewController {
    func setFilterView() -> FilterView {
        let filterV = FilterView(frame: .zero)
        filterV.translatesAutoresizingMaskIntoConstraints = false
        filterV.delegate = self
        filterV.backgroundColor = .white
        filterV.layer.cornerRadius = 8
        filterV.layer.masksToBounds = true
        return filterV
    }
    func setIndicatorView() -> UIActivityIndicatorView {
        let indicator = UIActivityIndicatorView(style: .large)
        indicator.translatesAutoresizingMaskIntoConstraints = false
        indicator.frame = .zero
        indicator.color = .red
        indicator.hidesWhenStopped = true
        return indicator
    }
    func setTableView() -> UITableView {
        let tblView = UITableView(frame: .zero, style: .plain)
        tblView.translatesAutoresizingMaskIntoConstraints = false
        tblView.showsVerticalScrollIndicator = false
        tblView.separatorStyle = .singleLine
        tblView.delegate = self
        tblView.dataSource = self
        return tblView
    }
}
