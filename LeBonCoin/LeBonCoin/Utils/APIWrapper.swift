//
//  APIWrapper.swift
//  LeBonCoin
//
//  Created by Malek Mansour on 09/02/2021.
//

import Foundation

protocol APIWrapperProtocol {
    static var listProductUrl: String { get }
    static var categoriesListUrl: String { get }
    static func downloadCategories(completionHandler: (@escaping([CategoryProtocol]) -> Void))
    static func getProductsList(completionHandler: (@escaping([ProductProtocol]) -> Void))
}
public class APIWrapper: APIWrapperProtocol {

    static let listProductUrl = "https://raw.githubusercontent.com/leboncoin/paperclip/master/listing.json"
    static let categoriesListUrl = "https://raw.githubusercontent.com/leboncoin/paperclip/master/categories.json"

    private static func dataTask<D: Decodable>(requestUrl: URL,
                                               completionHandler: (@escaping(Result<D, Error>) -> Void)) {
        URLSession.shared.dataTask(with: requestUrl) { (data, response, error) -> Void in
            if let data = data {
                guard let urlResponse = response as? HTTPURLResponse else { return}
                switch urlResponse.statusCode {
                case 200 ... 299:
                    let jsonDecoder = JSONDecoder()
                    do {
                        let item = try jsonDecoder.decode(D.self, from: data)
                        completionHandler(.success(item))
                    } catch let error {
                        completionHandler(.failure(error))
                    }
                case 401:
                    completionHandler(.failure(APIWrapperError.notAuthenticated))
                case 404:
                    completionHandler(.failure(APIWrapperError.notFound))
                default:
                    break
                }
            } else if let error = error {
                completionHandler(.failure(error))
            }
        }.resume()
    }

    class func downloadCategories(completionHandler: (@escaping([CategoryProtocol]) -> Void)) {
        if let url = URL(string: categoriesListUrl) {
            dataTask(requestUrl: url) { (result: Result<[Category], Error>) in
                switch result {
                case .success(let payload):
                    completionHandler(payload)
                case .failure(let error):
                    print(error)
                }
            }
        }
    }

    class func getProductsList(completionHandler: (@escaping([ProductProtocol]) -> Void)) {
        if let url = URL(string: listProductUrl) {
            dataTask(requestUrl: url) { (result: Result<[Product], Error>) in
                switch result {
                case .success(let payload):
                    completionHandler(payload)
                case .failure(let error):
                    print(error)
                }
            }
        }
    }
}
