//
//  Environment+Error.swift
//  LeBonCoin
//
//  Created by Malek Mansour on 09/02/2021.
//

import Foundation

// MARK: generic error handling
enum APIWrapperError: Swift.Error {

  /// Bad request sent to server 400
  case badRequest

  /// Bad token sent to server 401
  case notAuthenticated

  /// The server can not find requested resource. 404
  case notFound

  /// 300-399
  case redirects

  /// 400-499
  case clientError

  /// 500-599
  case serverError
}
extension APIWrapperError: LocalizedError {

  /// A localized message describing what error occurred.
  var errorDescription: String? {
    return nil
  }

  /// A localized message describing the reason for the failure.
  var failureReason: String? {
    return nil
  }

  /// A localized message describing how one might recover from the failure.
  var recoverySuggestion: String? {
    return nil
  }

  /// A localized message providing "help" text if the user requests help.
  var helpAnchor: String? {
    return nil
  }

}
